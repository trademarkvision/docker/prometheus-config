# Prometheus Config

[![pipeline status](https://gitlab.com/trademarkvision/docker/prometheus-config/badges/master/pipeline.svg)](https://gitlab.com/trademarkvision/docker/prometheus-config/commits/master)
[![Go Report](https://goreportcard.com/badge/gitlab.com/trademarkvision/docker/prometheus-config)](https://goreportcard.com/report/gitlab.com/trademarkvision/docker/prometheus-config)
