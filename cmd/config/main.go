package main

import (
	"fmt"
	"github.com/docker/leadership"
	"github.com/docker/libkv"
	"github.com/docker/libkv/store"
	"github.com/docker/libkv/store/consul"
	"github.com/mitchellh/go-homedir"
	"github.com/radovskyb/watcher"
	"github.com/sorintlab/pollon"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"gitlab.com/trademarkvision/docker/prometheus-config/internal/config"
	"log"
	"net"
	"regexp"
	"strings"
	"sync"
	"time"
)

func init() {
	initCLI()
	initLogging()
}

func main() {

	consul.Register()

	var client store.Store
	var err error
	if client, err = libkv.NewStore("consul", viper.GetStringSlice("consul-host"), &store.Config{}); err != nil {
		fmt.Println(err)
	}

	candidate := leadership.NewCandidate(
		client,
		fmt.Sprintf("service/%s/leader", viper.GetString("consul-service")),
		viper.GetString("consul-node"),
		viper.GetDuration("consul-ttl"),
	)

	var srcDirs []*config.Directory
	var dstDirs []*config.Directory

	src := viper.GetStringSlice("src")
	dst := viper.GetStringSlice("dst")

	for i, s := range src {
		sDir, err := config.New(s)
		if err != nil {
			fmt.Println(err)
		}

		dDir, err := config.New(dst[i])
		if err != nil {
			fmt.Println(err)
		}

		srcDirs = append(srcDirs, sDir)
		dstDirs = append(dstDirs, dDir)
	}

	w := watcher.New()
	w.IgnoreHiddenFiles(true)
	r := regexp.MustCompile(`^.*\.yml$`)
	w.AddFilterHook(watcher.RegexFilterHook(r, false))

	addr, err := net.ResolveTCPAddr("tcp", ":9093")
	if err != nil {
		log.Fatalf("error: %v", err)
	}
	listener, err := net.ListenTCP("tcp", addr)
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	proxy, err := pollon.NewProxy(listener)
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	wg := &sync.WaitGroup{}
	wg.Add(1)
	go func() {
		electedCh, errorCh := candidate.RunForElection()

		for {
			select {
			case isElected := <-electedCh:
				if isElected {
					log.Println("Leader")

					for i, src := range srcDirs {

						list, err := src.List()
						if err != nil {
							log.Fatalln(err)
						}

						for val := range config.FileExtensionFilterIterator(list, ".yml") {
							if err = src.Copy(val, dstDirs[i]); err != nil {
								log.Fatalln(err)
							}
						}

						if err = w.Add(src.Path); err != nil {
							log.Fatalln(err)
						}

						if viper.GetBool("prometheus-reload") {
							_, err := config.Reload(viper.GetString("prometheus-host"))
							if err != nil {
								log.Println(err)
							}
						}
					}

				} else {
					log.Println("Follower")
					for i, dst := range dstDirs {
						if err = dst.Empty(); err != nil {
							log.Fatalln(err)
						}

						if err = w.Remove(srcDirs[i].Path); err != nil {
							log.Fatalln(err)
						}

						if viper.GetBool("prometheus-reload") {
							_, err := config.Reload(viper.GetString("prometheus-host"))
							if err != nil {
								log.Println(err)
							}
						}
					}
				}
			case err := <-errorCh:
				if err != nil {
					fmt.Println("Election Error")
					for i, dst := range dstDirs {
						if err = dst.Empty(); err != nil {
							log.Fatalln(err)
						}

						if err = w.Remove(srcDirs[i].Path); err != nil {
							log.Fatalln(err)
						}

						if viper.GetBool("prometheus-reload") {
							_, err := config.Reload(viper.GetString("prometheus-host"))
							if err != nil {
								log.Println(err)
							}
						}
					}
					proxy.Stop()
				}
			}
		}
	}()

	wg.Add(1)
	go func() {
		go func() {
			for {
				select {
				case event := <-w.Event:
					config.HandleEvent(event, srcDirs, dstDirs)
				case err := <-w.Error:
					log.Fatalln(err)
				case <-w.Closed:
					return
				}
			}
		}()

		go func() {
			w.Wait()
		}()

		if err := w.Start(time.Millisecond * 100); err != nil {
			log.Fatalln(err)
		}
	}()

	wg.Add(1)
	go func() {
		err = proxy.Start()
		if err != nil {
			log.Fatalf("error: %v", err)
		}
	}()

	wg.Add(1)
	go func() {
		for {
			config.Check(proxy.C, client)
			time.Sleep(10 * time.Second)
		}
	}()

	wg.Wait()
}

func initLogging() {
	if viper.GetString("config") != "" {
		viper.SetConfigFile(viper.GetString("config"))
	} else {
		home, err := homedir.Dir()
		if err != nil {

		}
		viper.AddConfigPath(home)
		viper.SetConfigName("prom-config.yml")
	}

	viper.SetEnvPrefix("promcfg")
	viper.AutomaticEnv()

	replacer := strings.NewReplacer("-", "_")
	viper.SetEnvKeyReplacer(replacer)
}

func initCLI() {

	pflag.String("config", "", "config file (default is $HOME/prom-config.yml)")
	pflag.StringSlice("consul-host", []string{"localhost:8500"}, "comma separated list of consul hosts")
	pflag.String("consul-service", "prom-config", "name of service key to use in consul for sessions")
	pflag.Duration("consul-ttl", 60*time.Second, "ttl for consul lock on session")
	pflag.String("consul-node", "node", "unique name per node")
	pflag.StringSlice("src", []string{}, "sources")
	pflag.StringSlice("dst", []string{}, "destinations")
	pflag.Bool("prometheus-reload", false, "tell prometheus to reload its configs via the api")
	pflag.String("prometheus-host", "http://localhost:9090", "prometheus api uri (requires --web.enable-lifecycle)")

	pflag.Parse()

	err := viper.BindPFlags(pflag.CommandLine)
	if err != nil {
		fmt.Println("ERROR", err)
	}
}
