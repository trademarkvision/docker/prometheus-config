module gitlab.com/trademarkvision/docker/prometheus-config

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/docker/leadership v0.1.0
	github.com/docker/libkv v0.2.1
	github.com/hashicorp/consul v1.4.2 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.0 // indirect
	github.com/hashicorp/go-rootcerts v1.0.0 // indirect
	github.com/hashicorp/serf v0.8.2 // indirect
	github.com/mitchellh/go-homedir v1.0.0
	github.com/mitchellh/go-testing-interface v1.0.0 // indirect
	github.com/onsi/ginkgo v1.7.0
	github.com/onsi/gomega v1.4.3
	github.com/pkg/errors v0.8.1 // indirect
	github.com/radovskyb/watcher v1.0.6
	github.com/smartystreets/goconvey v0.0.0-20190222223459-a17d461953aa // indirect
	github.com/sorintlab/pollon v0.0.0-20181009091703-248c68238c16
	github.com/spf13/pflag v1.0.3
	github.com/spf13/viper v1.3.1
	github.com/stretchr/objx v0.1.1 // indirect
)
