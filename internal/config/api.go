package config

import (
	"io/ioutil"
	"net/http"
	"net/url"
	"path"
)

func Reload(host string) (string, error) {

	u, err := url.Parse(host)
	if err != nil {
		return "", err
	}

	u.Path = path.Join(u.Path, "/-/reload")
	response, err := http.Post(u.String(), "", nil)
	if err != nil {
		return "", err
	}

	data, _ := ioutil.ReadAll(response.Body)
	return string(data), nil
}
