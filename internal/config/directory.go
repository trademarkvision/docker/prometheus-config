package config

import (
	"errors"
	"io"
	"log"
	"os"
	"path/filepath"
)

var (
	NotADirectoryError = errors.New("file is not a directory")
)

type Directory struct {
	Path    string
	AbsPath string
	Info    os.FileInfo
}

func New(path string) (*Directory, error) {

	info, err := os.Stat(path)
	if err != nil {
		return nil, err
	}

	if !info.IsDir() {
		return nil, NotADirectoryError
	}

	abs, err := filepath.Abs(path)
	if err != nil {
		return nil, err
	}

	return &Directory{path, abs, info}, nil
}

func (src *Directory) Delete(name string) error {

	err := os.Remove(filepath.Join(src.Path, name))
	if err != nil {
		return err
	}

	log.Printf("Deleted %s from %s", name, src.Path)
	return nil
}

func (src *Directory) Rename(from, to string) error {

	err := os.Rename(filepath.Join(src.Path, from), filepath.Join(src.Path, to))
	if err != nil {
		return err
	}
	log.Printf("Renamed %s to %s", filepath.Join(src.Path, from), filepath.Join(src.Path, to))
	return nil
}

func (src *Directory) Copy(name string, dst *Directory) error {

	source, err := os.Open(filepath.Join(src.Path, name))
	if err != nil {
		return err
	}

	defer source.Close()

	destination, err := os.Create(filepath.Join(dst.Path, name))
	if err != nil {
		return err
	}

	defer destination.Close()

	bytes, err := io.Copy(destination, source)
	if err != nil {
		return err
	}

	log.Printf("Copied %s from %s to %s - %d", name, src.Path, dst.Path, bytes)

	return nil
}

func (src *Directory) List() ([]string, error) {

	s, err := os.Open(src.Path)
	if err != nil {
		return []string{}, err
	}

	names, err := s.Readdirnames(-1)
	if err != nil {
		return []string{}, err
	}

	return names, nil
}

func (src *Directory) Has(path string) bool {

	if _, err := os.Stat(filepath.Join(src.Path, path)); err == nil {
		return true
	}

	return false
}

func (src *Directory) Empty() error {
	dir, err := os.Open(src.Path)
	if err != nil {
		return err
	}

	names, err := dir.Readdirnames(-1)
	if err != nil {
		return err
	}

	for _, name := range names {
		err := os.RemoveAll(filepath.Join(src.Path, name))
		if err != nil {
			return err
		}
	}

	err = dir.Close()
	if err != nil {
		return err
	}
	return nil
}
