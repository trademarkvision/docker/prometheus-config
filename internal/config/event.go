package config

import (
	"github.com/radovskyb/watcher"
	"log"
	"path/filepath"
	"strings"
)

func HandleEvent(event watcher.Event, srcDirs []*Directory, dstDirs []*Directory) {

	if event.FileInfo.IsDir() {
		return
	}

	switch event.Op {
	case watcher.Create, watcher.Write:
		for i, src := range srcDirs {
			if strings.HasPrefix(event.Path, src.AbsPath) {
				err := src.Copy(event.FileInfo.Name(), dstDirs[i])
				if err != nil {
					log.Fatalln(err)
				}
			}
		}

	case watcher.Remove:
		for i, src := range srcDirs {
			if strings.HasPrefix(event.Path, src.AbsPath) {
				err := dstDirs[i].Delete(event.FileInfo.Name())
				if err != nil {
					log.Fatalln(err)
				}
			}
		}
	case watcher.Rename:
		paths := strings.Split(event.Path, " -> ")
		for i, src := range srcDirs {
			if strings.HasPrefix(paths[0], src.AbsPath) {

				err := dstDirs[i].Rename(filepath.Base(paths[0]), filepath.Base(paths[1]))
				if err != nil {
					log.Fatalln(err)
				}
			}
		}
	}
}
