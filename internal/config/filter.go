package config

import "path/filepath"

func FileExtensionFilterIterator(list []string, ext string) <-chan string {
	ch := make(chan string)
	go func() {
		for _, val := range list {
			if filepath.Ext(val) == ext {
				ch <- val
			}
		}
		close(ch)
	}()
	return ch
}
