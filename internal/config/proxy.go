package config

import (
	"fmt"
	"github.com/docker/libkv/store"
	"github.com/sorintlab/pollon"
	"log"
	"net"
	"strings"
)

func Check(c chan pollon.ConfData, store store.Store) {

	kv, err := store.Get("service/prom-config/leader")
	if err != nil {
		log.Println(err)
		c <- pollon.ConfData{DestAddr: nil}
		return
	}

	master := fmt.Sprintf("%s.servers.tmv.cloud:9090", kv.Value)

	addrStr := strings.TrimSpace(string(master))
	_, _, err = net.SplitHostPort(addrStr)
	if err != nil {
		log.Printf("err: %v", err)
		c <- pollon.ConfData{DestAddr: nil}
		return
	}
	addr, err := net.ResolveTCPAddr("tcp", addrStr)
	if err != nil {
		log.Printf("error resolving address: %v", err)
		c <- pollon.ConfData{DestAddr: nil}
		return
	}

	c <- pollon.ConfData{DestAddr: addr}
}
